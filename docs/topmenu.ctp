<?php
/*
Element para criar um menu que exibe somente links para controllers aos quais o usuário tem permissão de acesso.
*/
	if($loguser == 'super'){
        print '<ul>';
		foreach($supers as $controller){
			echo '<li><h5>'.$this->Html->link(__($controller[0]), array('plugin'=>null,'controller'=>$controller[0],'action'=>'index')).'</h5></li>';
		}        
        print '</ul>';
	}elseif($loguser == 'admin'){
        print '<ul>';
		foreach($admins as $controller){
			echo '<li><h5>'.$this->Html->link(__($controller[0]), array('plugin'=>null,'controller'=>$controller[0],'action'=>'index')).'</h5></li>';
		}
        print '</ul>';
	}elseif($loguser == 'manager'){
        print '<ul>';
		foreach($managers as $controller){
			echo '<li><h5>'.$this->Html->link(__($controller[0]), array('plugin'=>null,'controller'=>$controller[0],'action'=>'index')).'</h5></li>';
		}
        print '</ul>';
	}elseif($loguser == 'user'){
        print '<ul>';
		foreach($users as $controller){
			echo '<li><h5>'.$this->Html->link(__($controller[0]), array('plugin'=>null,'controller'=>$controller[0],'action'=>'index')).'</h5></li>';
		}
        print '</ul>';
	}
