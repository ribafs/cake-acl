Plugin para Implementar ACL no CakePHP 3
===========================================

[![Licença](https://img.shields.io/packagist/l/doctrine/orm.svg?maxAge=2592000)](https://github.com/ribafs/cake-control-br/blob/master/LICENSE)

URL deste projeto - https://github.com/ribafs/cake-acl/

Este plugin inclue o plugin BootstrapUI e o (Twitter) Bootstrap e também inclui a estrutura do plugin [twbs-cake-plugin](https://github.com/elboletaire/twbs-cake-plugin).

## AVISO

**Funciona somente com as versões:**

    PHP 7.2
    CakePHP 3.5.13 (https://github.com/cakephp/cakephp/releases/download/3.5.13/cakephp-3-5-13.zip) Ainda não está compatível com a versão 3.6


## Principais recursos
    Template do bake traduzido para pt_BR
    Menu de topo usando o element topmenu
    Busca com paginação
    Senhas criptografadas com Bcrypt
    Controle de Acesso/ACL com administração web
    Layout customizado

## Instalação e uso
https://github.com/ribafs/cake-acl

Criar app:
    Baixar o cake 3.5.13 daqui:
    https://github.com/cakephp/cakephp/releases/download/3.5.13/cakephp-3-5-13.zip
    Ou outra versão anterior. Enquanto o suporte ao Cake atual no sai. 
    Descompacte na pasta
    /var/www/html/acl1

## Instalar Plugin

    cd /var/www/html/acl1
    composer require ribafs/cake-acl

## Configurar banco

Crie o banco e importe o script existente na pasta docs do plugin baixado. Depois edite config/app.php para configurar o banco.

Aproveite e configure também o controller default em config/routes.php para um de seu interesse.

## Habilitar o plugin

    bin/cake plugin load CakeAcl --bootstrap

## Download do plugin

    https://github.com/ribafs/cake-acl/archive/master.zip

## Descompactar e abrir o sub-diretório docs, então copiar:

- cake.css para webroot/css no raiz do aplicativo
- pdo_error.ctp para src/Template/Error
- AppController.php para src/Controller (sobrescrevendo)
- bootstrap_cli.php para config/ (sobrescrevendo)
- default.ctp para src/Tempalte/Layout (sobrescrevendo)
- topmenu.ctp para src/Tempalte/Element (sobrescrevendo)

    cd control1
    bin/cake bake all groups -t CakeAcl
    bin/cake bake all users -t CakeAcl
    bin/cake bake all permissions -t CakeAcl
    bin/cake bake all customers -t CakeAcl

Existem 4 usuários, cada um com permissões diferentes:

- super - com senha super também tem total permissão em tudo.
- admin - com senha admin tem total permissão nas tabelas groups, users e permissions.
- manager - com senha manager tem total permissão somente nas tabelas diferentes de groups, users e permissions.
- user - com senha user não tem nenhuma permissão no aplicativo, apenas de efetuar login.

Após terminar experimente cada um dos usuários e veja como funciona o plugin ACL. Cada usuário está limitado apenas ao que foi definido para ele.

Em AppController.php você pode definir o controller default para usuários não administradores. Caso não use a tabela customers troque logo no início do AppController por uma de suas tabelas na linha:

    protected $noAdmins = 'Customers';


# Documentação
Alguns detalhes a mais - https://ribafs.github.io/cakephp/cake-control.pdf


## Sugestão
Para plugin com ACL e Bootstrap use

https://github.com/ribafs/cake-acl-br


## Sugestões, colaborações e forks serão muto bem vindos:

- Erros: português
- PHP
- CakePHP
- ControlComponent.php
- etc

License
-------

The MIT License (MIT)
